/*A. Theatre Square
 * Para hacer este ejercicio hay que saber cuántas baldosas a x a caben a lo
 * ancho y alto del teatro. Para ello se dividen las dos dimensiones n y m del
 * teatro entre el lado de la baldosa, y se saca el redondeo hacia arriba (no 
 * podemos partir las baldosas). Despues, multiplicando los valores obtenidos 
 * de las divisiones obtenemos el número de baldosas necesario.
 */
var theatre = readline().split(' ');
print(Math.ceil(theatre[0]/theatre[2]) * Math.ceil(theatre[1]/theatre[2]));



/*A. Watermelon
 * Para hacer este ejercicio, tenemos que saber si el resto de la división entre
 * 2 del peso dará cero, es decir, si es divisible entre 2. Además, tenemos que 
 * comprobar que el peso no sea 2, pues el resto daría cero pero el peso dividido
 * sería impar, sin posibilidad de repartir (por ejemplo, 30/2 son 15. Puedes 
 * hacer trozos de 14 y 16, pero con 2/2 es 1, no puedes repartir 2 y 0).
 */
var w = parseInt(readline());
if (w%2===0 && w!==2) {
	print('YES');
} else {
	print('NO');
}

/*A. Way Too Long Words
 * Para hacer este ejercicio hay que saber cuántas palabras vamos a cambiar,
 * guardando el valor en una variable que usamos para saber cuántas veces 
 * ejecutamos el bucle. Después, acada línea le aplicamos lo mismo: si es más
 * larga de 10 caracteres devolvemos la primera letra (word[0]), el número de
 * letras centrales eliminadas (todas, menos 2 word.length-2) y la última letra
 * (word[word.length-1]). Si es más corta, la devolvemos tal cual.
 */
var lines = readline();
for (var i = 0; i < lines; i++) {
	var word = readline();
	if (word.length > 10) {
		print(word[0]+(word.length-2)+word[word.length-1]);
	} else {
		print(word);
	}
}

/*A. Next Round
 * Para hacer este ejercicio tenemos que leer dos arrays, uno con los valores n y k,
 * y otro con las puntuaciones. Guardamos el valor de k en otra variable, para 
 * operar más cómodamente, y guardamos también la puntuación del participante k en 
 * otra variable independiente, por comodidad. A continuación, haremos un bucle que
 * recorra el array de puntuaciones, y que incremente un contador (count) siempre
 * y cuando la puntuación que mire en ese momento sea igual o mayor que la del
 * participante k, y mayor de 0. Devolvemos el contador.
 */
var contestants = readline().split(' ');
var scores = readline().split(' ');
var count = 0;
var k = contestants[1]-1;
var scoreK = parseInt(scores[k]);
for (var i = 0; i < scores.length; i++) {
	if (parseInt(scores[i]) >= scoreK) {
      	if (scores[i] > 0) {
        	count++;
      	}
	}
}
print(count);

/*A. String Task
 * Para hacer este ejercicio tenemos que devolver el input de entrada, pero sin
 * vocales, en minúsculas y con un punto delante de cada consonante. Para ello lo
 * primero que hacemos es cambiar a minúsculas el string de entrada, luego miramos
 * letra a letra del string y si es vocal, no hacemos nada, mientras que si es 
 * consonante, concatenamos un punto y la consonante en la variable edited. Al
 * final devolvemos la variable edited.
 */
var string = readline();
var edited = '';
string = string.toLowerCase();

for (var i = 0; i < string.length; i++) {
	if (string.charAt(i) != 'a' &&
        string.charAt(i) != 'e' &&
        string.charAt(i) != 'i' &&
        string.charAt(i) != 'o' &&
        string.charAt(i) != 'u' &&
        string.charAt(i) != 'y') {
			
        edited += '.'+string[i];
	}
}
print(edited);

/*A. Domino piling
 * Para hacer este ejercicio tenemos que saber cuántas fichas de dominó nos 
 * caben en el cartón M x N, sabiendo que una ficha de dominó ocupa 2 cuadrados
 * (2x1). Es suficiente con averiguar el área del cartón (M x N), dividirla entre
 * dos (el área de una ficha), y redondear hacia abajo (para no partir las fichas, 
 * ni salirse del cartón)
*/
var board = readline().split(' ');
print(Math.floor((board[0]*board[1])/2));

/*A. Team
 * Para hacer este ejercicio en primer lugar hay que saber el número de problemas
 * que hay en el campeonato. Luego, hay que ver si saben resolverlos. Para ello, 
 * cada array problem sumado tiene que ser mayor de 1 (dos o más) porque si no, 
 * los participantes no responden. Aumentamos la variable count cuando se cumple
 * la condición y la devolvemos al final.
 */
var lines = readline();
var count = 0;
for (var i = 0; i < lines; i++) {
	var problem = readline().split(' ');
	if (parseInt(problem[0])+parseInt(problem[1])+parseInt(problem[2]) > 1) {
		count++;
	}
}
print(count);

/*A. Bit++
* Para hacer este ejercicio debemos saber cuántas órdenes se van a dar, para 
* ejecutar el bucle ese número de veces. Después, vemos si la orden es de sumar
* o no. Si es de sumar (++X o X++) sumamos uno a x (inicialmente, 0). En caso 
* contrario, le restamos uno. Finalmente, imprimimos el valor de x.
*/
var lines = readline();
var x = 0;
for (var i = 0; i < lines; i++) {
	var line = readline().split(' ');
	if (line == '++X' || line == 'X++') {
		x++;
	} else {
		x--;
	}
}
print(x);

/*A. Football
 * Para hacer este ejericio tenemos que saber cuántos números iguales hay juntos.
 * Para ello, nos creamos una variable dangerous igual a 1 (que representa el 
 * primer posible jugador al lado de otro de su equipo) y miramos si en el array
 * de entrada la observación actual y la siguiente son iguales. En ese caso, 
 * incrementamos la variable dangerous en 1 (ya habría 2 jugadores iguales seguidos)
 * en caso contrario, la volvemos a poner a 1 y miramos la observación siguiente. Y 
 * así hasta finalizar el string de entrada. Finalmente, si dangeours llega a 7 en
 * algún momento, devolvemos 'YES', si no devolvemos 'NO'.
 */
var situation =readline();
var dangerous = 1;
for(var i=0; i<situation.length; i++) {
    if(situation.charAt(i) === situation.charAt(i+1)) {
    	dangerous++;
    } else {
    	dangerous = 1;
    }
    
    if (dangerous === 7){
    	print("YES");
    	break;
    }
}

if(dangerous !== 7) {
	print("NO");
} 

/*A. Petya and Strings
 * Para hacer este ejercicio tenemos que saber qué string es "mayor" sin tener
 * en cuenta las mayúsculas/minúsculas. Así que podemos pasar los dos string a 
 * minúsculas y javascript nos permite evaluarlos directamente como iguales, 
 * mayores o menores. En cada uno de los casos, devolvemos lo que nos dice
 * el problema.
 */
var string1 = readline().toLowerCase();
var string2 = readline().toLowerCase();

if (string1 === string2) {
	print(0);
} else if (string1 > string2) {
	print(1);
} else if (string1 < string2) {
	print(-1)
}


/*A. Helpful Maths
 * Para hacer este ejercicio tenemos que ordenar los números que recibimos en
 * orden creciente. Para ello, dividimos el input de entrada por los "+", 
 * ordenamos el array resultante, y lo devolvemos como string separado por "+"
 * nuevamente con la función join.
 */
print(readline().split('+').sort().join('+'));


/*A. Tram
 * Para hacer este ejercicio tenemos que ir restando los pasajeros que salen 
 * del tranvía y sumar a los que entran. Para ello, yo he decidido transformar
 * los input de entrada en dos array distintos. Uno con las salidas, y otro con
 * las entradas (simplemente asignando la primera posición del input a entradas y
 * la segunda al de salidas). Con esos array, voy calculando las personas que 
 * entran y salen, y si el resultado de las que quedan es mayor que la variable
 * sum, me guardo ese valor para devolverlo al final.
 */
var lines = readline();
lines = parseInt(lines);
var exits= new Array();
var entries = new Array();
var sum = 0;
var persons = 0;

for (var i=0, j=0; i<lines; i++) {
	var line = readline().split(' ');
	exits.push(parseInt(line[j]));
	entries.push(parseInt(line[j+1]));
}

for (var z=0; z<entries.length; z++) {
	if (persons+entries[z]-exits[z] > sum) {
		sum = persons+parseInt(entries[z])-parseInt(exits[z]);
	}
	persons = persons+parseInt(entries[z])-parseInt(exits[z]);
}

print(sum);


/*A. cAPS lOCK
 * Para hacer este ejercicio tenemos que saber, en primer lugar, si la primera
 * letra está en minúsculas. En tal caso, hacemo true la variable firstLetter.
 * En caso contrario, sumamos uno a la variable count. Después, evaluamos si el
 * resto de letras son mayúsculas, y por cada una sumamos uno a la variable count.
 * Al final hacemos dos evaluaciones: si la primera letra es minúscula y todas las
 * demás mayúsculas, devolvemos el string tal como nos pide el problema (primera en
 * mayúscula, las demás minúsculas). Si todas son mayúsculas (count == string.length)
 * devolvemos el string en minúsculas. Si no se cumplen estas condiciones, devolvemos
 * el string tal cual lo hemos recibido.
 */
var string = readline();
var changed = "";
var count = 0;
var firstLetter = false;

for (var i=0; i<string.length; i++) {
	if (i < 1) {
		if (string.charAt(i) == string.charAt(i).toLowerCase()) {
			firstLetter = true;
		} else {
			count++;
		}
	} else {
		if (string.charAt(i) == string.charAt(i).toUpperCase()) {
			count++;
		}
	}
}

if (count == string.length-1 && firstLetter) {
	for (var i=0; i<string.length; i++) {
		if (i < 1) {
			changed = string.charAt(i).toUpperCase();
		} else {
			changed += string.charAt(i).toLowerCase();
		}
	}
} else if (count == string.length) {
	for (var i=0; i<string.length; i++) {
			changed += string.charAt(i).toLowerCase();
	}
} else {
	changed = string;
}

print(changed);

/*A. Stones on the Table
 * Para hacer este ejercicio tenemos que evitar que dos piedras iguales se
 * queden juntas. Para ello, cada vez que una posición del string de colors
 * y el siguiente sean iguales, sumamos uno a la variable count. Finalmente
 * devolvemos la variable count que representa el número de piedras iguales
 * que hay que quitar.
 */
var stones = +readline();
var colors = readline();
var count = 0;

for (var i=0; i<colors.length; i++) {
	if (colors[i] == colors[i+1]) {
		count++;
	}
}

print(count);